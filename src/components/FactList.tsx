import React from 'react';
import Fact from './Fact';
import '../styles/fact-list.css';

function FactList() {
  const factCount = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

  return (
    <div className="fact-list">
      {factCount.map((fact, index) => (
        <Fact key={fact} />
      ))}
    </div>
  );
}

export default FactList;
