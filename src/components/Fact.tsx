import {useEffect, useState} from 'react';
import '../styles/fact.css';
import axios from 'axios';

function Fact() {
  const [fact, setFact] = useState('loading Chuck fact...');
  const [imageUrl, setImageUrl] = useState('');

  useEffect(() => {
    axios.get('https://api.chucknorris.io/jokes/random').then((response) => {
      const {data} = response;
      setFact(data.value);
    });
  }, []);

  useEffect(() => {
    let num = Math.floor(Math.random() * 10 + 1);
    setImageUrl(`/src/assets/img/${num}.jpeg`);
  }, []);

  return (
    <div className="fact">
      <div
        className="fact__image"
        style={{backgroundImage: 'url(' + imageUrl + ')'}}
      />
      <p>{fact}</p>
    </div>
  );
}

export default Fact;
