import './styles/App.css';
import FactList from './components/FactList';

function App() {
  return (
    <div className="app">
      <h1>Chuck Norris Facts</h1>
      <FactList></FactList>
    </div>
  );
}

export default App;
